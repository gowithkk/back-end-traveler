#!/bin/bash

sudo chmod -R 777 /home/ec2-user/bestravellers

#navigate into our working directory where we have all out bitbucket files
cd /home/ec2-user/bestravellers

#add npm and node to path
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" # loads nvm bash_completion

#install node modules
npm ci

#start out node app in the backend
CONNECTION_STRING='mongodb+srv://admin:Lk50935317@cluster0.bpykf.mongodb.net/myFirstDatabase?retryWrites=true&w=majority' PORT=8000 npm start > app.out.log 2> app.err.log < /dev/null &